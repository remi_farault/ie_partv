#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>

#include <libpartvcom/server.h>
#include <libpartvcom/com.h>
#include <libpartvcom/data.h>
#include <libpartvcom/client.h>
#include <libpartvcom/.h>
#include <libpartvcom/error.h>

#include <libpartvthread/csv.h>
#include <libpartvthread/tcp_client.h>
#include <libpartvthread/tcp_server.h>
#include <libpartvthread/serial.h>


#include "args.h"


static bool g_run = true;

void stop(int sig)
{
    switch(sig)
    {
        case SIGINT:
        case SIGKILL:
        case SIGSTOP:
            g_run = false;
            break;
        default:
            break;
    }
}

int main(int argc, char **argv)
{
    args_t arguments;
    pthread_t network_thread, csv_thread;
    dist_mat_queue_t *common_queue = NULL;
    tcp_server_args_t server_args;
    csv_thread_arg_t csv_args;

    /* get call arguments */
    if ( get_args(argc, argv, &arguments) != 0 )
    {
        fprintf(stderr, "main: get_args error\n");
        goto error;
    }

#if 0
    /* daemonize */
    if ( daemon(0, 0) != 0 )
    {
        fprintf(stderr, "main: daemon error\n");
        goto error;
    }
#endif
    
    /* initialize threads common matrix queue */
    common_queue = calloc(1, sizeof(dist_mat_queue_t));
    if ( common_queue == NULL )
    {
        fprintf(stderr, "main: allocate error\n");
        goto error;
    }

    /* empty the queue and its size */
    memset(common_queue, 0, sizeof(*common_queue));

    /* detach threads */
    pthread_create(&network_thread, NULL, &tcp_server_run, (void *) &server_args);
    pthread_create(&csv_thread, NULL, &csv_run, (void *) &csv_args);

    /* wait */
    pthread_join(network_thread, NULL);
    pthread_join(csv_thread, NULL);

    destruct_queue(common_queue);

    return EXIT_SUCCESS;

/* at error instructions */
error:

    return EXIT_FAILURE;
}