#ifndef _ARGS_H
#define _ARGS_H

#include <stdint.h>

typedef struct args_s
{
    uint16_t port;
}
args_t;

int get_args(int argc, char **argv, args_t *ptr);

#endif