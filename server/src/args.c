#include "args.h"
#include <getopt.h>
#include <stdlib.h>
#include <stdint.h>

int get_args(int argc, char **argv, args_t *ptr)
{
    int c;
    static struct option long_options[]  = 
    {
        { "port", required_argument, 0, 'p' }
    };

    do
    {
        c = getopt_long(argc, argv, "p:",long_options, NULL);
        
        switch(c)
        {
            case 'p':
                ptr->port = (uint16_t) strtol(optarg, NULL, 10);
                break;
            default:
                break;
        }
    }
    while( c != -1 );

    return 0;
}
