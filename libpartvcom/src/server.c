/**
 * @file server.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

/* libc */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

/* linux */
#include <sys/socket.h>
#include <netinet/in.h>

/* local */
#include <libpartvcom/client.h>
#include <libpartvcom/data.h>
#include <libpartvcom/com.h>
#include <libpartvcom/error.h>
#include <libpartvcom/server.h>

/**
 * @brief create a new tcp server
 * 
 * @param[out] pserver double pointer to created server (must not be NULL)
 * @param[in] port tcp port to listen on
 * @return error code
 */
int new_server(partv_server_t **pserver, uint16_t port)
{
    assert(pserver != NULL);

    partv_server_t *server = NULL;
    struct sockaddr_in tcp_host;
    int fd;

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if ( fd < 0 )
    {
        fprintf(stderr, "new_server: socket error\n");
        goto error;
    }

    memset(&tcp_host, 0, sizeof(tcp_host));
    tcp_host.sin_family = AF_INET;
    tcp_host.sin_addr.s_addr = htonl(INADDR_ANY);
    tcp_host.sin_port = htons(port);

    if ( bind(fd, &tcp_host, sizeof(tcp_host)) != 0 )
    {
        fprintf(stderr, "new_server: bind error\n");
        goto error;
    }

    server = calloc(1, sizeof(partv_server_t));
    if ( server == NULL )
    {
        fprintf(stderr, "new_server: allocate error\n");
        goto error;
    }

    server->sockfd = fd;
    memset(server->clients, 0, sizeof(server->clients));

    listen(fd, MAX_CLIENTS);

    *pserver = server;

    return EXIT_SUCCESS;

error:
    if ( fd > 0 ) close(fd);
    return EXIT_FAILURE;
}

/**
 * @brief close connections and destruct server
 * 
 * @param[in] server (must not be NULL)
 * @return error code
 */
int close_server(partv_server_t *server)
{
    assert(server != NULL);
    
    for(int i=0; i<server->clients_cnt; i++)
    {
        close(server->clients[i].sockfd);
    }

    close(server->sockfd);
    free(server);

    return EXIT_SUCCESS;
}

/**
 * @brief Accept a new client and stores its information
 * 
 * @param[in] server (must not be NULL)
 * @return error code 
 */
int accept_client(partv_server_t *server)
{
    partv_client_info_t client;
    struct sockaddr_in tcp_remote;
    int fd, socklen, ret;

    assert(server != NULL);
    
    socklen = sizeof(tcp_remote);

    fd = accept(server->sockfd, &tcp_remote, &socklen);
    if ( fd < 0 )
    {
        fprintf(stderr, "accept_client: accept error\n");
        goto error;
    }

    if ( server->clients_cnt == MAX_CLIENTS-1 )
    {
        int err = CLIENT_ID_SERVER_FULL;

        if ( send (fd, &err, sizeof(err), 0) != sizeof(err) )
        {
            fprintf(stderr, "accept_client: send err error\n");
            goto error;
        }

        return EXIT_SUCCESS;
    }

    int id = server->clients_cnt + 1;

    if ( send (fd, &id, sizeof(id), 0) != sizeof(id) )
    {
        fprintf(stderr, "accept_client: send id error\n");
        ret = COM_ERROR_INTERNAL;
        goto error;
    }

    client.addr = tcp_remote;
    client.sockfd = fd;
    client.id = server->clients_cnt;

    server->clients[server->clients_cnt++] = client;

    return COM_ERROR_NONE;

error:
    if ( fd > 0 ) close(fd);
    return ret;

}
