/**
 * @file com.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief Send/receive functions that can be used over socket or serial line
 * 
 */

/* libc */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

/* local */
#include <libpartvcom/client.h>
#include <libpartvcom/data.h>
#include <libpartvcom/com.h>
#include <libpartvcom/error.h>

#define TOKEN ';'

/* to remove if possible */
int recv_char_mat(dist_mat_t **pmat, int fd)
{
    assert(pmat != NULL);

    dist_mat_t *mat = calloc(1, sizeof(dist_mat_t));
    int size = 0;
    char *token;

    char buffer[PACKET_SIZE];

    size = read(fd, buffer, PACKET_SIZE-1);
    
    for (int i = 0; i < SIZE; i++)
    {   
        token = i == 0 ? strtok(buffer, TOKEN) : strtok(NULL, TOKEN);
        mat->array[i] = (uint32_t) strtol(token, NULL, 10);
    }

    return EXIT_SUCCESS;
}

/**
 * @brief receive a distance matrix over socket or serial line
 * 
 * Allocate the matrix, make sure to free it later
 * 
 * @param[out] pmat double pointer of new matrix created (must not be NULL)
 * @param[in] fd file descriptor to write
 * @return error code
 */
int recv_raw_mat(dist_mat_t **pmat, int fd)
{
    assert(pmat != NULL);
    
    dist_mat_t *mat = calloc(1, sizeof(dist_mat_t));
    int size = 0;

    size = read(fd, mat->array, sizeof(mat->array));

    if ( size != sizeof(mat->array) )
    {
        fprintf(stderr, "read_raw_mat: invalid read size %d\n", size);
        goto error;
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}

/**
 * @brief send a distance matrix over socket or serial line
 * 
 * 
 * @param[out] mat pointer to matrix to send (must not be NULL)
 * @param[in] fd file descriptor to write
 * @return error code
 */
int send_raw_mat(dist_mat_t *mat, int fd)
{
    assert(mat != NULL);

    if ( write(fd, mat, sizeof(*mat), 0) != sizeof(*mat) )
    {
        fprintf(stderr, "send_request: send failed\n");
        goto error;
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}