/**
 * @file data.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

/* libc */
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>

/* local */
#include <libpartvcom/client.h>
#include <libpartvcom/data.h>
#include <libpartvcom/com.h>
#include <libpartvcom/error.h>

/**
 * @brief add a cell on queue tail
 * 
 * @param[in] queue (must not be NULL)
 * @param[in] mat (must not be NULL)
 * @return error code
 */
int enqueue(dist_mat_queue_t *queue, dist_mat_t *mat)
{
    cell_t *cell = NULL;
    assert(queue != NULL);
    assert(mat != NULL);

    if ( queue->size == MAX_QUEUE )
    {
        fprintf(stderr, "enqueue: queue is full\n");
        goto error;
    }

    cell = calloc(1, sizeof(cell_t));
    cell->mat = mat;

    if ( queue->size == 0 ) /* queue is empty */
    {
        queue->start = cell;
        queue->end = cell;
        queue->size++;
    }
    else
    {
        cell->next = queue->start;
        queue->start = cell;
        queue->size++;
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}

/**
 * @brief remove a cell from queue head
 * 
 * @param[out] pmat (must not be NULL)
 * @param[in] queue (must not be NULL)
 * @return error code
 */
int dequeue(dist_mat_t **pmat, dist_mat_queue_t *queue)
{
    cell_t *cell = NULL;

    assert(queue != NULL);
    assert(pmat != NULL);

    if ( queue->size == 0 )
    {
        goto error;
    }

    cell = queue->end;
    
    if (queue->size == 1)
    {
        queue->end = NULL;
        queue->start = NULL;
    }
    else 
    {
        queue->end = cell->prev;
    }

    queue->size--;

    *pmat = cell->mat;

    free(cell);

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}


/**
 * @brief Create a queue object (allocate it)
 * 
 * @param[out] pqueue double pointer to created queue (must not be NULL)
 * @return error code 
 */
int create_queue(dist_mat_queue_t **pqueue)
{
    assert(pqueue != NULL);

    dist_mat_queue_t *queue = calloc(1, sizeof(dist_mat_queue_t));
    if ( queue == NULL )
    {
        fprintf(stderr, "create_queue: alloc error\n");
        goto error;
    }

    *pqueue = queue;

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}

/**
 * @brief Empty and destruct a queue object (free it)
 * 
 * @param[in] queue 
 * @return error code
 */
int destruct_queue(dist_mat_queue_t *queue)
{
    assert(queue != NULL);

    while ( queue->start != queue->end )
    {
        queue->start = queue->start->next;
        free(queue->start->prev->mat);
        free(queue->start->prev);
    }
    free(queue->end->mat);
    free(queue->end);
    free(queue);

    return EXIT_SUCCESS;
}