/**
 * @file client.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief Client functions for libpartvcom
 * 
 * 
 */

/* libc */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

/* linux */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

/* local */
#include <libpartvcom/client.h>
#include <libpartvcom/data.h>
#include <libpartvcom/com.h>
#include <libpartvcom/error.h>


/**
 * @brief Create a new client and connects it to a server
 * 
 * @param[out] pclient pointer to client struct (must not be NULL)
 * @param[in] server_addr ip address of server to connect to
 * @param[in] port server port
 * @return error code 
 */
int new_client(partv_client_t **pclient, const char *server_addr, int port)
{
    partv_client_t *client = NULL;
    struct sockaddr_in tcp_host;
    int fd, ret;

    fd = socket(AF_INET, SOCK_STREAM, 0);   /* create tcp client socket */
    if ( fd < 0 )
    {
        fprintf(stderr, "new_client: socket error\n");
        ret = COM_ERROR_INTERNAL;
        goto error;
    }

    /* initialize host address/port */
    memset(&tcp_host, 0, sizeof(tcp_host));
    tcp_host.sin_family = AF_INET; 
    tcp_host.sin_addr.s_addr = inet_addr(server_addr);
    tcp_host.sin_port = htons(port);
    
    /* connect socket to host */
    if ( connect(fd, &tcp_host, sizeof(tcp_host)) != 0 )
    {
        fprintf(stderr, "new_client: connect error\n");
        ret = COM_ERROR_INTERNAL;
        goto error;
    }

    /* reception of our client id */
    int id = 0;
    if ( recv(fd, &id, sizeof(id), 0) != sizeof(id) )
    {
        fprintf(stderr, "new_client: recv id error\n");
        ret = COM_ERROR_INTERNAL;
        goto error;
    }

    /* test value of id */
    if ( id < 0 || id == CLIENT_ID_SERVER_FULL )
    {
        fprintf(stderr, "new_client: bad id value (server is probably full)\n");
        ret = COM_ERROR_BAD_ID;
        goto error;
    }

    /* allocate client structure */
    client = calloc(1, sizeof(partv_client_t));
    if ( client == NULL )
    {
        fprintf(stderr, "new_client: allocate error\n");
        ret = COM_ERROR_INTERNAL;
        goto error;   
    }

    /* fill client structure */
    client->sockfd = fd;
    client->id = id;

    *pclient = client;

    return COM_ERROR_NONE;

    /* cleanup & exit error */
error:
    if ( fd > 0 ) close(fd);
    if ( client ) free(client);
    return ret;
}

/**
 * @brief Disconnect and destruct the client
 * 
 * @param[in] client pointer to client (must not be NULL)
 * @return error code
 */
int close_client(partv_client_t *client)
{
    close(client->sockfd);
    free(client);
    
    return COM_ERROR_NONE;
}

