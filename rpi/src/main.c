#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <termios.h>

#include "args.h"


#include <libpartvcom/server.h>
#include <libpartvcom/com.h>
#include <libpartvcom/data.h>
#include <libpartvcom/client.h>
#include <libpartvcom/error.h>

#include <libpartvthread/csv.h>
#include <libpartvthread/tcp_client.h>
#include <libpartvthread/tcp_server.h>
#include <libpartvthread/raw_serial.h>

static bool g_run = true;

void stop(int sig)
{
    switch(sig)
    {
        case SIGINT:
        case SIGKILL:
        case SIGSTOP:
            g_run = false;
            break;
        default:
            break;
    }
}

int main(int argc, char **argv)
{
    args_t arguments;
    
    pthread_t serial_thread, csv_thread;
    raw_serial_thread_arg_t serial_args;
    csv_thread_arg_t csv_args;
    
    dist_mat_queue_t *common_queue = NULL;
    
    if ( get_args(argc, argv, &arguments) != 0 )
    {
        fprintf(stderr, "main: get_args error\n");
        goto error;
    }

    /* initialize threads common matrix queue */
    common_queue = calloc(1, sizeof(dist_mat_queue_t));
    if ( common_queue == NULL )
    {
        fprintf(stderr, "main: allocate error\n");
        goto error;
    }

    /* empty the queue and its size */
    memset(common_queue, 0, sizeof(*common_queue));

    serial_args.shared_queue = common_queue;
    csv_args.shared_queue = common_queue;
    csv_args.filename = arguments.file;
    serial_args.device = arguments.dev;
    serial_args.baudrate = B115200;
    serial_args.shared_queue = common_queue;

    pthread_create(&serial_thread, NULL, &raw_serial_run, (void *) &serial_args);
    pthread_create(&csv_thread, NULL, &csv_run, (void *) &csv_args);

    pthread_join(serial_thread, NULL);
    pthread_join(csv_thread, NULL);

    destruct_queue(common_queue);

    return EXIT_SUCCESS;

/* at error instructions */
error:
    if ( common_queue ) destruct_queue(common_queue);
    
    return EXIT_FAILURE;
}