#include <stdint.h>

#ifndef _ARGS_H
#define _ARGS_H


typedef struct args_s
{
    uint16_t port;
    const char *addr;
    const char *dev;
    const char *file;
}
args_t;

int get_args(int argc, char **argv, args_t *ptr);

#endif