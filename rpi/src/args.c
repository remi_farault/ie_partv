#include "args.h"
#include <getopt.h>
#include <stdlib.h>

int get_args(int argc, char **argv, args_t *ptr)
{
    int c;
    static struct option long_options[]  = 
    {
        { "port", required_argument, 0, 'p' },
        { "address", required_argument, 0, 'a' },
        { "device", required_argument, 0, 'd' },
        { "file", required_argument, 0, 'f' }
    };

    do
    {
        c = getopt_long(argc, argv, "a:p:d:f:",long_options, NULL);
        
        switch(c)
        {
            case 'a':
                ptr->addr = optarg;
                break;
            case 'p':
                ptr->port = strtol(optarg, NULL, 10);
                break;
            case 'd':
                ptr->dev = optarg;
                break;
            case 'f':
                ptr->file = optarg;
                break;
            default:
                break;
        }
    }
    while( c != -1 );

    return 0;
}
