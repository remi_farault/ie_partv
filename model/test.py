import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

# Charger et préparer les données d'entraînement
data = pd.read_csv('data_complete.csv')
X = data.drop('Etiquette', axis=1)
y = data['Etiquette']
X_train, y_train = X, y

# Entraîner le modèle
knn = KNeighborsClassifier(n_neighbors=3)
knn.fit(X_train, y_train)

# Charger les données de test
df_test = pd.read_csv('data_test.csv')
X_test = df_test.values

# Faire des prédictions
y_pred = knn.predict(X_test)

# Afficher les prédictions
print("Prédictions :")
print(y_pred)

