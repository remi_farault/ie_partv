#

## libpartvcom

TCP Communication library to transmit matrixes between rpi & server

## rpi

source dir of raspberry pi daemon

## server 

source dir of server daemon

## model

AI model directory

## stm32

Custom embedded sources for STM32

## webapp

web application source dir