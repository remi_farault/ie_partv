import socket
import sys


PORT = int(sys.argv[1])

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(('', PORT))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print("new conn")
        while True:
            data = conn.recv(1024)
            if not data:
                break
            print(data)