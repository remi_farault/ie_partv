/**
 * @file tcp_client.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <assert.h>

#include <libpartvcom/com.h>
#include <libpartvthread/tcp_client.h>

/**
 * @brief create a new tcp client
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int tcp_client_init(tcp_client_args_t *args)
{
    assert(args != NULL);

    new_client(&args->client, args->addr, args->port);
    if ( args->client == NULL )
    {
        fprintf(stderr, "tcp_client_init: new client error\n");
        goto error;
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}

/**
 * @brief close tcp client
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int tcp_client_exit(tcp_client_args_t * args)
{
    assert(args != NULL);

    close_client(args->client);

    return 0;
}

/**
 * @brief tcp client thread main loop
 * 
 * read a matrix from threads shared queue and send it to tcp server
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int tcp_client_run(void *args)
{
    tcp_client_args_t *client_args = NULL;
    dist_mat_t *mat = NULL;
    struct timespec tv;

    assert(args != NULL);

    client_args = (tcp_client_args_t *) args;

    tv.tv_sec = 0;
    tv.tv_nsec = 1000;

    while( true )
    {
        if ( client_args->shared_queue->size > 0 )
        {
            dequeue(&mat, client_args->shared_queue);
            if ( mat == NULL )
            {
                fprintf(stderr, "tcp_client_run: dequeue error\n");
                goto error;
            }
            
            if ( send_raw_mat(client_args->fd, mat) != 0 )
            {
                fprintf(stderr, "tcp_client_run: send matrix error\n");
                goto error;
            }

            free(mat);
            
        }
        nanosleep(&tv, NULL);
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}