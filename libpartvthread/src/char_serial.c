#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <termios.h>

#include <sys/epoll.h>

#include <libpartvcom/data.h>
#include <libpartvcom/com.h>

#include <libpartvthread/char_serial.h>

int char_serial_init(char_serial_thread_arg_t *args)
{
    assert(args != NULL);
    
    struct termios tio;
    struct epoll_event ev;

    args->serialfd = open(args->device, 0);
    if ( args->serialfd < 0 )
    {
        fprintf(stderr, "serial_init: open error\n");
        goto error;
    }

    memset(&tio, 0, sizeof(tio));
    tio.c_iflag = 0;
    tio.c_oflag = 0;
    tio.c_cflag = CS8|CREAD|CLOCAL;           
    tio.c_lflag = 0;
    tio.c_cc[VMIN] = 1;
    tio.c_cc[VTIME] = 96;

    cfsetospeed(&tio, args->baudrate);           
    cfsetispeed(&tio, args->baudrate);            

    tcsetattr(args->serialfd, TCSANOW, &tio);

    args->epoll_fd = epoll_create(1);

    ev.events = EPOLLIN|EPOLLET;
    ev.data.fd = args->serialfd;

    if ( epoll_ctl(args->epoll_fd, EPOLL_CTL_ADD, args->serialfd, &ev) < 0 )
    {
        fprintf(stderr, "serial_run: epoll_ctl_add error\n");
        goto error;
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}

int char_serial_exit(char_serial_thread_arg_t *args)
{
    assert(args != NULL);

    close(args->serialfd);

    return EXIT_SUCCESS;
}

int char_serial_run(void *args)
{
    dist_mat_t *mat = NULL;
    char_serial_thread_arg_t *serial_args = NULL;
    struct epoll_event ev;
    struct timespec tv;
    int nfd;

    assert(args != NULL);

    serial_args = (char_serial_thread_arg_t *) args;

    if ( char_serial_init(serial_args) != EXIT_SUCCESS )
    {
        fprintf(stderr, "serial_run: serial_init error\n");
        goto error;
    }

    tv.tv_sec = 0;
    tv.tv_nsec = 1000;

    while ( true )
    {
        if ( read_char_mat(&mat, serial_args->serialfd) != EXIT_SUCCESS )
        {
            fprintf(stderr, "serial_run: read_mat error\n");
            goto error;
        }

        if ( enqueue(serial_args->shared_queue, mat) != EXIT_SUCCESS )
        {
            fprintf(stderr, "serial_run: enqueue error\n");
            goto error;
        }
        
        nanosleep(&tv, NULL);
    }      

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}
