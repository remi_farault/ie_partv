/**
 * @file csv.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <stdbool.h>
#include <assert.h>

#include <libpartvcom/data.h>

#include <libpartvthread/csv.h>

/**
 * @brief open csv file 
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int csv_init(csv_thread_arg_t *args)
{
    assert(args != NULL);

    args->csvfile = fopen(args->filename, "w");
    if ( args->csvfile == NULL )
    {
        fprintf(stderr, "csv_init: open error\n");
        goto error;
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}

/**
 * @brief close csv file
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int csv_exit(csv_thread_arg_t *args)
{
    assert(args != NULL);

    fclose(args->csvfile);
    
    return EXIT_SUCCESS;
}

/**
 * @brief csv thread main loop
 * 
 * read a matrix on thread shared queue and write it on csv file 
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int csv_run(void * args)
{
    dist_mat_t *mat = NULL;
    csv_thread_arg_t *csv_args = NULL;
    struct timespec tv;
    
    assert(args != NULL);

    csv_args = (csv_thread_arg_t *) args;

    if ( csv_init(csv_args) != EXIT_SUCCESS )
    {
        fprintf(stderr, "csv_run: csv_init error\n");
        goto error;
    }
    
    tv.tv_sec = 0;
    tv.tv_nsec = 1000;
    
    while(true)
    {
        if ( csv_args->shared_queue->size > 0 )
        {
            
            dequeue(&mat, csv_args->shared_queue);

            if ( mat == NULL )
            {
                fprintf(stderr, "csv_run: dequeue error\n");
                goto error;
            }

            for (int i = 0; i < SIZE; i++)
            {
                    
                fprintf (csv_args->csvfile, "%d;",mat->array[i]);
            }

            fprintf(csv_args->csvfile,"\n");

            free(mat);
        }
        nanosleep(&tv, NULL);
    }

    return EXIT_SUCCESS;

error: 
    return EXIT_FAILURE;    
}
