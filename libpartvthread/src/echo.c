#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <libpartvcom/client.h>
#include <libpartvcom/data.h>
#include <libpartvcom/com.h>
#include <libpartvcom/response.h>
#include <libpartvcom/error.h>

#include <libpartvthread/echo.h>

static void print_mat(dist_mat_t *mat)
{
    for (int i = 0; i < SIZE; i++)
    {
        printf("%d ;", mat->array[i]);
    }
    printf("\n");
}

void echo_run(void *arg)
{
    int fd = *((int *) arg);
    dist_mat_t *mat = NULL;
    struct timespec tv;

    tv.tv_sec = 0;
    tv.tv_nsec = 1000;

    while ( true )
    {
        if ( read_char_mat(&mat, fd) != EXIT_SUCCESS )
        {
            fprintf(stderr, "serial_run: read_mat error\n");
            return;
        }

        print_mat(mat);

        free(mat);

        nanosleep(&tv, 0);
    }
}