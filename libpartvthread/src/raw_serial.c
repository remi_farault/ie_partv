/**
 * @file raw_serial.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <termios.h>

#include <sys/epoll.h>

#include <libpartvcom/data.h>
#include <libpartvcom/com.h>

#include <libpartvthread/raw_serial.h>
 
/**
 * @brief init raw serial tty and its epoll file descriptor
 * 
 * @param args 
 * @return error code
 */
int raw_serial_init(raw_serial_thread_arg_t *args)
{
    assert(args != NULL);
    
    struct termios tio;
    struct epoll_event ev;

    args->serialfd = open(args->device, 0);
    if ( args->serialfd < 0 )
    {
        fprintf(stderr, "serial_init: open error\n");
        goto error;
    }

    memset(&tio, 0, sizeof(tio));
    tcgetattr(args->serialfd, &tio);

    cfmakeraw(&tio);
    cfsetspeed(&tio, (speed_t) args->baudrate);           

    tcsetattr(args->serialfd, TCSANOW, &tio);

    args->epoll_fd = epoll_create(1);

    ev.events = EPOLLIN|EPOLLET;
    ev.data.fd = args->serialfd;

    if ( epoll_ctl(args->epoll_fd, EPOLL_CTL_ADD, args->serialfd, &ev) < 0 )
    {
        fprintf(stderr, "serial_run: epoll_ctl_add error\n");
        goto error;
    }

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}

/**
 * @brief close serial line
 * 
 * @param[in] args thread arguments structure
 * @return error code 
 */
int raw_serial_exit(raw_serial_thread_arg_t *args)
{
    assert(args != NULL);

    close(args->serialfd);
    
    return EXIT_SUCCESS;
}

/**
 * @brief serial thread main loop
 * 
 * receive a matrix on serial line and write it to thread shared queue
 * 
 * @param args 
 * @return error code
 */
int raw_serial_run(void *args)
{
    dist_mat_t *mat = NULL;
    raw_serial_thread_arg_t *serial_args = NULL;
    struct epoll_event ev;
    struct timespec tv;
    int nfd;

    assert(args != NULL);

    serial_args = (raw_serial_thread_arg_t *) args;

    if ( raw_serial_init(serial_args) != EXIT_SUCCESS )
    {
        fprintf(stderr, "serial_run: serial_init error\n");
        goto error;
    }

    while ( true )
    {
        nfd = epoll_wait(serial_args->epoll_fd, &ev, 1, -1);
        if ( nfd > 0 )
        {
            if ( read_raw_mat(&mat, serial_args->serialfd) != EXIT_SUCCESS )
            {
                fprintf(stderr, "serial_run: read_mat error\n");
                goto error;
            }

            if ( enqueue(serial_args->shared_queue, mat) != EXIT_SUCCESS )
            {
                fprintf(stderr, "serial_run: enqueue error\n");
                goto error;
            }
        }
    }      

    return EXIT_SUCCESS;

error:
    return EXIT_FAILURE;
}
