/**
 * @file tcp_server.c
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <sys/epoll.h>

#include <libpartvthread/tcp_server.h>

/**
 * @brief create a tcp server and its epoll file descriptor
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int tcp_server_init(tcp_server_args_t *args)
{
   struct epoll_event ev;
   assert(args != NULL);

   /* initialize the server */
   new_server(&args->server, args->port);
   if ( args->server == NULL )
   {
      fprintf(stderr, "main: new server error\n");
      goto error;
   }

   /* initialize epoll for server fd */
   args->epfd = epoll_create(MAX_CLIENTS+1);
   if ( args->epfd < 0 )
   {
      fprintf(stderr, "new_server: epoll_create error\n");
      goto error;
   }

   ev.events = EPOLLIN | EPOLLET;
   ev.data.fd = args->server->sockfd;

   if( epoll_ctl(args->epfd, EPOLL_CTL_ADD, args->server->sockfd, &ev) < 0 )
   {
      fprintf(stderr, "new_server: epoll_ctl_add error.");
      goto error;
   }

   return EXIT_SUCCESS;

error:

   return EXIT_FAILURE;
}

/**
 * @brief close tcp server
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int tcp_server_exit(tcp_server_args_t *args)
{
   assert(args != NULL);

   close_server(args->server);
   
   return 0;
}

/**
 * @brief tcp server thread main loop
 * 
 * receive matrixes sent by clients and store them on threads shared queue
 * 
 * @param[in] args thread arguments structure
 * @return error code
 */
int tcp_server_run(void *args)
{
   int nfds;
   struct timespec tv;
   tcp_server_args_t *server_args = NULL;
   dist_mat_t *mat = NULL;
   struct epoll_event evs[MAX_CLIENTS+1];
   struct epoll_event ev;

   assert(args != NULL);

   /* get thread arguments */
   server_args = (tcp_server_args_t *) args;

   /* thread main loop */
   while ( true )
   {
      memset(evs, 0, sizeof(evs));

      /* listen epoll fd */
      nfds = epoll_wait(server_args->epfd, evs, MAX_CLIENTS+1, -1);
      if ( nfds < 0 )
      {
         fprintf(stderr, "server_run: epoll_wait error\n");
         goto error;
      }

      fprintf(stdout, "server_run: cnt is %d\n", nfds);

      for ( int i = 0; i < nfds; i++ )
      {
         if ( evs[i].data.fd == server_args->server->sockfd )
         {
            if ( accept_client(server_args->server) != EXIT_SUCCESS )
            {
               fprintf(stderr, "server_run: accept_client error\n");
               goto error;
            }

            /* add client fd to epoll fd*/
            memset(&ev, 0, sizeof(ev));

            ev.events = EPOLLIN | EPOLLET;
            ev.data.fd = server_args->server->clients[server_args->server->clients_cnt].sockfd; //sorry for this :/

            if ( epoll_ctl(server_args->epfd, EPOLL_CTL_ADD, server_args->server->sockfd, &ev) < 0 )
            {
               fprintf(stderr, "server_run: epoll_ctl_add error\n");
               goto error;
            }
         }
         else
         {
            if ( read_raw_mat(&mat, server_args->server->sockfd) != EXIT_SUCCESS )
            {
                fprintf(stderr, "server_run: read_mat error\n");
                goto error;
            }

            if ( enqueue(server_args->shared_queue, mat) != EXIT_SUCCESS )
            {
                fprintf(stderr, "server_run: enqueue error\n");
                goto error;
            }
         }
      }
   }

   return EXIT_SUCCESS;

error:
   return EXIT_FAILURE;
}