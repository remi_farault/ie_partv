.PHONY: clean

COM = libpartvcom
THREAD = libpartvthread
RPI = rpi
SERVER = server

clean: 
	find . -name "*.o"  | xargs rm -f
	rm -r build/*
