#include "data.h"

#ifndef _COM_H
#define _COM_H

#define INT_SIZE 5
#define PACKET_SIZE 96

int read_char_mat   (dist_mat_t **pmat, int fd);
int read_raw_mat    (dist_mat_t **pmat, int fd);
int send_raw_mat    (dist_mat_t *mat, int fd)
#endif