#include <stdint.h>

#ifndef _DATA_H
#define _DATA_H

#define SIZE 16
#define MAX_QUEUE 32

/**
 * @brief distance matrix structure
 * 
 */
typedef struct dist_mat_s
{
    uint32_t array[SIZE];   /** distances array */
}
dist_mat_t;

/**
 * @brief dist_mat_t queue cell structure
 * 
 */
struct cell_s
{
    dist_mat_t *mat;        /** distance matrix */
    struct cell_s *prev;    /** previous cell */
    struct cell_s *next;    /** next cell */
};

typedef struct cell_s cell_t;

/**
 * @brief dist_mat_t queue structure
 * 
 */
typedef struct dist_mat_queue_s
{
    cell_t *start;          /** queue tail */
    cell_t *end;            /** queue head */
    int size;               /** queue size */
}
dist_mat_queue_t;

int enqueue         (dist_mat_queue_t *queue, dist_mat_t *mat);
int dequeue         (dist_mat_t **pmat, dist_mat_queue_t *queue);
int destruct_queue  (dist_mat_queue_t *queue);

#endif