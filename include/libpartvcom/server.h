#include <stdint.h>
#include <netinet/in.h>

#ifndef _SERVER_H
#define _SERVER_H

#define MAX_CLIENTS 100
#define SERVER_TIMEOUT 100

/**
 * @brief client information structure
 * 
 */
typedef struct partv_client_info_s
{
    int sockfd;                                         /** client socket */
    int id;                                             /** client id */
    struct sockaddr_in addr;                            /** client ip address */
}
partv_client_info_t;

/**
 * @brief server structure
 * 
 */
typedef struct partv_server_s
{
    int sockfd;                                         /** server main socket */
    int clients_cnt;                                    /** number of connected clients */
    struct sockaddr_in addr;                            /** server ip address */
    struct partv_client_info_s clients[MAX_CLIENTS];    /** client info array */
}
partv_server_t;

int new_server      (partv_server_t **pserver, uint16_t port);
int close_server    (partv_server_t *server);
int accept_client   (partv_server_t *server);

#endif