#ifndef _ERROR_H
#define _ERROR_H

#define CLIENT_ID_SERVER_FULL 0

enum
{
    COM_ERROR_NONE          =  0,
    COM_ERROR_INTERNAL      = -1,
    COM_ERROR_SERVER_FULL   = -2,
    COM_ERROR_BAD_ID        = -3,
    COM_BAD_RESPONSE
};

#endif