/**
 * @file client.h
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include "data.h"

#ifndef _CLIENT_H
#define _CLIENT_H

/**
 * @brief client structure
 * 
 */
typedef struct partv_client_s
{
    int id;     /** client id sent by server */
    int sockfd; /** tcp socket */
}
partv_client_t;

int new_client      (partv_client_t **pclient, const char *server_addr, int port);
int close_client    (partv_client_t *client);

#endif
