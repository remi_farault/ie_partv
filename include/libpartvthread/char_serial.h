/**
 * @file char_serial.h
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <libpartvcom/data.h>

#ifndef _CHAR_SERIAL_H
#define _CHAR_SERIAL_H


typedef struct char_serial_thread_arg_s
{
    int baudrate;
    int serialfd;
    int epoll_fd;
    const char *device;
    dist_mat_queue_t *shared_queue;
}
char_serial_thread_arg_t;

int char_serial_init(char_serial_thread_arg_t *args);
int char_serial_exit(char_serial_thread_arg_t *args);
int char_serial_run(void *serial_args);

#endif