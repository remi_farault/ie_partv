/**
 * @file raw_serial.h
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <libpartvcom/data.h>

#ifndef _RAW_SERIAL_H
#define _RAW_SERIAL_H

/**
 * @brief serial reader thread arguments
 * 
 */
typedef struct raw_serial_thread_arg_s
{
    int baudrate;                   /** serial line baudrate */
    int serialfd;                   /** file descriptor of serial line */
    int epoll_fd;                   /** epoll file descriptor */
    const char *device;             /** serial line device path */
    dist_mat_queue_t *shared_queue; /** thread shared queue */
}
raw_serial_thread_arg_t;

int raw_serial_init(raw_serial_thread_arg_t *args);
int raw_serial_exit(raw_serial_thread_arg_t *args);
int raw_serial_run(void *serial_args);

#endif