/**
 * @file tcp_client.h
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <libpartvcom/data.h>
#include <libpartvcom/client.h>

#ifndef _TCP_CLIENT_H
#define _TCP_CLIENT_H

/**
 * @brief tcp client thread arguments
 * 
 */
typedef struct tcp_client_args_t
{
    uint16_t port;                      /** server port */
    const char *addr;                   /** server address */
    partv_client_t *client;             /** client structure */
    dist_mat_queue_t *shared_queue;     /** thread shared queue */
}
tcp_client_args_t;

int tcp_client_init(tcp_client_args_t *args);
int tcp_client_exit(tcp_client_args_t *args);
int tcp_client_run(void *args);

#endif