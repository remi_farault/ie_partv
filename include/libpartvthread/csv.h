#include <libpartvcom/data.h>
/**
 * @file csv.h
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <stdio.h>

#ifndef _CSV_H
#define _CSV_H

/**
 * @brief csv writer thread arguments
 * 
 */
typedef struct csv_thread_arg_s
{
    FILE *csvfile;                  /** csv file pointer*/
    const char *filename;           /** csv file path */
    dist_mat_queue_t *shared_queue; /** thread shared queue */
}
csv_thread_arg_t;

int csv_init    (csv_thread_arg_t *args);
int csv_exit    (csv_thread_arg_t *args);
int csv_run     (void *args);


#endif