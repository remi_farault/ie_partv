/**
 * @file tcp_server.h
 * @author Rémi Farault (remi.farault@gmail.com)
 * @brief 
 * 
 * 
 */

#include <libpartvcom/server.h>
#include <libpartvcom/data.h>
#include <libpartvcom/com.h>

#ifndef _TCP_SERVER_H
#define _TCP_SERVER_H

/**
 * @brief tcp server thread arguments
 * 
 */
typedef struct tcp_server_args_s
{
    uint16_t port;                  /** server tcp port */
    int epfd;                       /** epoll file descriptor */
    partv_server_t *server;         /** server structure */
    dist_mat_queue_t *shared_queue; /** thread shared queue */
}
tcp_server_args_t;

int tcp_server_init(tcp_server_args_t *args);
int tcp_server_exit(tcp_server_args_t *args);
int tcp_server_run(void *args);

#endif